resource "null_resource" "figlet" {
  triggers = {
    version = timestamp()
  }

  provisioner "local-exec" {
    command = "figlet ${var.figlet_text} > ${path.module}/output.txt"
  }
}

data "local_file" "figlet_output" {
  filename = "${path.module}/output.txt"

  depends_on = [null_resource.figlet]
}
